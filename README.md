ESLint configuration for JavaScript based qnzl projects and modules.

## Status
This project is in alpha development.

## Dependencies
- [Node](https://nodejs.org/en/download/)

## Usage
in `package.json`:
```js
"devDependencies": {
  "eslint": "^4.19.1",
  "eslint-config-qnzl": "gitlab:qnzl/eslint-config-qnzl",
  "eslint-plugin-import": "^2.12.0"
}
```

## See Also
- [ESLint](https://eslint.org/)

## License
LGPL-3.0
